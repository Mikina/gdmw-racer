using UnrealBuildTool;

public class EndlessRacerTarget : TargetRules
{
	public EndlessRacerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("EndlessRacer");
	}
}
